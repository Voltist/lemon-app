var lat;
var lon;

function center() {
  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '50%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });
}

function send( code) {

  navigator.geolocation.getCurrentPosition(function(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;

    $.ajax({
        method: "POST",
        url: "https://lemon.artifxl.com/api/add",
        data: {
          "code": code,
          "lat": lat,
          "lon": lon
        }
      })
      .done(function(data) {
        if (data["success"]) {

          alert("Success! Thanks for contributing :)");
          navigator.app.exitApp();

        } else {
          console.log(data["error"]);
        }

      });

  }, function() {
    alert("Can't get geolocation! Are location services turned on?");
  });
}

document.addEventListener('deviceready', function() {
  cordova.plugins.barcodeScanner.scan(
      function (result) {
          send(result.text);
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
          prompt : "Place a Lime QR Code inside the scan area",
          formats : "QR_CODE"
      }
   );
});
